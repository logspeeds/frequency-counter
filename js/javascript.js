
document.getElementById("countButton").onclick = function () {
    //Modo de descobrir o que foi digitado na caixa de texto
    let typedText = document.getElementById("textInput").value;
    const letterCounts = {};
    let wordCounts = {};
    let words = typedText.split(/\s/);
    console.log(words)

    typedText = typedText.toLowerCase();
    // Isto muda todas as letras para minúsculas
    typedText = typedText.replace(/[^a-z'\s]+/g, "");
    // Isso se livra de todos os caracteres exceto letras comuns, espaços e apóstrofos. 
    // Iremos aprender mais sobre como us// Iremos aprender mais sobre como usar a função replace numa lição mais à frente.

    for (let i = 0; i < typedText.length; i++) {
        currentLetter = typedText[i];
        if (letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1;
        } else {
            letterCounts[currentLetter]++;
        }
    }

    for (let z = 0; z <words.length; z++) {
        let currentWord = words[z];
        if (wordCounts[currentWord] === undefined) {
            wordCounts[currentWord] = 1;
        } else {
            wordCounts[currentWord]++        
        }
    }

    for (let letter in letterCounts) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
        span.appendChild(textContent);
        document.getElementById("lettersDiv").appendChild(span);
    }

    for (let word in wordCounts) {
        const span2 = document.createElement("span");
        const textContent2 = document.createTextNode('"' + word + "\": " + wordCounts[word] + ", ");
        span2.appendChild(textContent2);
        document.getElementById("wordsDiv").appendChild(span2);
    }
}
